#![feature(let_chains)]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

use core::str;
use std::borrow::Cow;
use std::ffi::{CStr, CString};
use std::fmt::Debug;
use std::os::raw::{c_char, c_int};
use std::sync::OnceLock;
use std::sync::atomic::{AtomicBool, Ordering};
use std::{io, mem};

use thiserror::Error;

include!(concat!(env!("OUT_DIR"), "/deadbeef_bindings.rs"));

const MAX_LEN: usize = 256;

static IS_PLUGIN_ENABLED: AtomicBool = AtomicBool::new(false);
static DEADBEEF: OnceLock<&DB_functions_t> = OnceLock::new();

fn format() -> Result<Cow<'static, str>, NowPlayingError> {
    let now_playing = PlayingTrack::new()?;
    if now_playing.0.is_null() {
        return Ok(Cow::Borrowed("no song playing"));
    }

    let now_playing_playlist = PlayingPlaylist::new()?;

    let format_string = DEADBEEF.conf_get_str(
        c"now_playing_to_file.format",
        c"%title%$if(%ispaused%,' ('paused')')",
    )?;

    let script = FormatScript::new(&format_string)?;
    if script.0.is_null() {
        return Ok(Cow::Borrowed(""));
    }

    let out = DEADBEEF.tf_eval(now_playing, now_playing_playlist, script)?;

    let out = unsafe { Cow::Owned(String::from(CStr::from_ptr(out.as_ptr()).to_str()?)) };

    Ok(out)
}

fn write_to_file() -> Result<(), NowPlayingError> {
    let path = DEADBEEF.conf_get_str(c"now_playing_to_file.out_path", c"")?;
    if path.is_empty() || path[0] == 0 {
        return Ok(());
    }

    let path = unsafe { CStr::from_ptr(path.as_ptr()).to_str()? };
    std::fs::write(path, &*format()?)?;

    Ok(())
}

#[unsafe(no_mangle)]
unsafe extern "C" fn now_playing_to_file_start() -> c_int {
    IS_PLUGIN_ENABLED.store(DEADBEEF.plugin_enabled().unwrap_or(true), Ordering::Relaxed);

    0
}

#[unsafe(no_mangle)]
unsafe extern "C" fn now_playing_to_file_stop() -> c_int {
    IS_PLUGIN_ENABLED.store(false, Ordering::Relaxed);

    0
}

#[unsafe(no_mangle)]
unsafe extern "C" fn now_playing_to_file_message(
    id: u32,
    _ctx: usize,
    _p1: u32,
    _p2: u32,
) -> c_int {
    match id {
        DB_EV_SONGCHANGED | DB_EV_PAUSED | DB_EV_STOP => {
            if IS_PLUGIN_ENABLED.load(Ordering::Relaxed) {
                if let Err(e) = write_to_file() {
                    DEADBEEF.log_error(e);
                }
            }
        }
        DB_EV_CONFIGCHANGED => {
            IS_PLUGIN_ENABLED.store(DEADBEEF.plugin_enabled().unwrap_or(true), Ordering::Relaxed);
        }
        _ => (),
    };

    0
}

const CONFIG: &CStr = cr#"
property "Enable"      checkbox now_playing_to_file.enable   1;
property "Format"      entry    now_playing_to_file.format   "%title%$if(%ispaused%,' ('paused')')";
property "Output path" entry    now_playing_to_file.out_path "";
"#;

const START: Option<unsafe extern "C" fn() -> c_int> = Some(now_playing_to_file_start);
const STOP: Option<unsafe extern "C" fn() -> c_int> = Some(now_playing_to_file_stop);
const MESSAGE: Option<unsafe extern "C" fn(u32, usize, u32, u32) -> c_int> =
    Some(now_playing_to_file_message);
const PLUGIN: DB_misc_t = DB_misc_t {
    plugin: DB_plugin_t {
        type_: DB_PLUGIN_MISC as i32,
        api_vmajor: 1,
        api_vminor: 16,
        id: c"now_playing_to_file".as_ptr(),
        name: c"Now Playing to File Plugin".as_ptr(),
        descr: c"Outputs a formatted string based on your current playing track to a file of your choice.".as_ptr(),
        copyright:
        c"Now Playing to File Plugin for DeaDBeeF
Copyright (C) 2024 Tassad <Tassadaritze@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
".as_ptr(),
        website: c"https://gitlab.com/Tassad/deadbeef_now_playing_to_file".as_ptr(),
        start: START,
        stop: STOP,
        message: MESSAGE,
        configdialog: CONFIG.as_ptr(),
        version_major: 1,
        version_minor: 2,
        flags: 0,
        reserved1: 0,
        reserved2: 0,
        reserved3: 0,
        command: None,
        connect: None,
        disconnect: None,
        exec_cmdline: None,
        get_actions: None,
    },
};

#[unsafe(no_mangle)]
pub fn now_playing_to_file_load(api: &'static DB_functions_t) -> &'static DB_plugin_t {
    if DEADBEEF.set(api).is_err() {
        DEADBEEF.log("warning: api was already set on plugin");
    }

    &PLUGIN.plugin
}

trait TryApi {
    fn try_get_api(&self) -> Result<&DB_functions_t, NowPlayingError>;
    fn plugin_enabled(&'static self) -> Result<bool, NowPlayingError>;
    fn conf_get_str(
        &self,
        key: &CStr,
        default: &CStr,
    ) -> Result<[c_char; MAX_LEN], NowPlayingError>;
    fn tf_eval(
        &self,
        now_playing: PlayingTrack,
        now_playing_playlist: PlayingPlaylist,
        script: FormatScript,
    ) -> Result<[c_char; MAX_LEN], NowPlayingError>;
    fn log(&self, message: impl AsRef<str>);
    fn log_error(&self, error: NowPlayingError);
}

impl TryApi for OnceLock<&DB_functions_t> {
    fn try_get_api(&self) -> Result<&DB_functions_t, NowPlayingError> {
        match self.get() {
            Some(val) => Ok(val),
            None => Err(NowPlayingError::ApiUninit),
        }
    }

    fn plugin_enabled(&'static self) -> Result<bool, NowPlayingError> {
        unsafe {
            Ok(self
                .try_get_api()?
                .conf_get_int
                .ok_or(NowPlayingError::ConfGetIntUnavailable)?(
                c"now_playing_to_file.enable".as_ptr(),
                0,
            ) == 1)
        }
    }

    fn conf_get_str(
        &self,
        key: &CStr,
        default: &CStr,
    ) -> Result<[c_char; MAX_LEN], NowPlayingError> {
        let mut buf = [0; MAX_LEN];
        unsafe {
            self.try_get_api()?
                .conf_get_str
                .ok_or(NowPlayingError::ConfGetStrUnavailable)?(
                key.as_ptr(),
                default.as_ptr(),
                buf.as_mut_ptr(),
                MAX_LEN as c_int,
            );
        }

        Ok(buf)
    }

    fn tf_eval(
        &self,
        now_playing: PlayingTrack,
        now_playing_playlist: PlayingPlaylist,
        script: FormatScript,
    ) -> Result<[c_char; MAX_LEN], NowPlayingError> {
        let mut context = ddb_tf_context_t {
            _size: mem::size_of::<ddb_tf_context_t>() as c_int,
            flags: 0,
            it: now_playing.0,
            plt: now_playing_playlist.0,
            idx: 0,
            id: 0,
            iter: PL_MAIN as c_int,
            update: 0,
            dimmed: 0,
            metadata_transformer: None,
        };

        let mut buf = [0; MAX_LEN];
        unsafe {
            self.try_get_api()?
                .tf_eval
                .ok_or(NowPlayingError::TfEvalUnavailable)?(
                &mut context,
                script.0,
                buf.as_mut_ptr(),
                MAX_LEN as c_int,
            );
        }

        Ok(buf)
    }

    fn log(&self, message: impl AsRef<str>) {
        match CString::new(format!("now_playing_to_file: {}\n", message.as_ref())) {
            Ok(message) => match self.try_get_api().ok().and_then(|api| api.log) {
                Some(log) => unsafe {
                    log(message.as_ptr());
                },
                None => eprintln!("{message:?}"),
            },
            Err(_) => eprintln!("now_playing_to_file: {}", message.as_ref()),
        };
    }

    fn log_error(&self, error: NowPlayingError) {
        match CString::new(format!("now_playing_to_file: {error:?}\n")) {
            Ok(message) => match self.try_get_api().ok().and_then(|api| api.log) {
                Some(log) => unsafe {
                    log(message.as_ptr());
                },
                None => eprintln!("{message:?}"),
            },
            Err(_) => eprintln!("now_playing_to_file: {error:?}"),
        };
    }
}

#[repr(transparent)]
struct PlayingTrack(*mut ddb_playItem_t);

impl PlayingTrack {
    fn new() -> Result<Self, NowPlayingError> {
        unsafe {
            Ok(Self(DEADBEEF
                .try_get_api()?
                .streamer_get_playing_track_safe
                .ok_or(
                    NowPlayingError::StreamerGetPlayingTrackSafeUnavailable,
                )?()))
        }
    }
}

impl Drop for PlayingTrack {
    fn drop(&mut self) {
        if !self.0.is_null()
            && let Some(api) = DEADBEEF.get()
            && let Some(pl_item_unref) = api.pl_item_unref
        {
            #[cfg(debug_assertions)]
            DEADBEEF.log(format!("dropping PlayingTrack ref to {:?}", self.0));

            unsafe {
                pl_item_unref(self.0);
            }
        } else {
            debug_assert!(self.0.is_null());
        }
    }
}

#[repr(transparent)]
struct PlayingPlaylist(*mut ddb_playlist_t);

impl PlayingPlaylist {
    fn new() -> Result<Self, NowPlayingError> {
        unsafe {
            Ok(Self(DEADBEEF
                .try_get_api()?
                .plt_get_curr
                .ok_or(NowPlayingError::PltGetCurrUnavailable)?(
            )))
        }
    }
}

impl Drop for PlayingPlaylist {
    fn drop(&mut self) {
        if !self.0.is_null()
            && let Some(api) = DEADBEEF.get()
            && let Some(plt_unref) = api.plt_unref
        {
            #[cfg(debug_assertions)]
            DEADBEEF.log(format!("dropping PlayingPlaylist ref to {:?}", self.0));

            unsafe {
                plt_unref(self.0);
            }
        } else {
            debug_assert!(self.0.is_null());
        }
    }
}

#[repr(transparent)]
struct FormatScript(*mut c_char);

impl FormatScript {
    fn new(buf: &[c_char]) -> Result<Self, NowPlayingError> {
        unsafe {
            Ok(Self(DEADBEEF
                .try_get_api()?
                .tf_compile
                .ok_or(NowPlayingError::TfCompileUnavailable)?(
                buf.as_ptr(),
            )))
        }
    }
}

impl Drop for FormatScript {
    fn drop(&mut self) {
        if !self.0.is_null()
            && let Some(api) = DEADBEEF.get()
            && let Some(tf_free) = api.tf_free
        {
            #[cfg(debug_assertions)]
            DEADBEEF.log(format!("freeing FormatScript at {:?}", self.0));

            unsafe {
                tf_free(self.0);
            }
        } else {
            debug_assert!(self.0.is_null());
        }
    }
}

#[derive(Error, Debug)]
enum NowPlayingError {
    #[error("api must be initialized at this point")]
    ApiUninit,
    #[error("conf_get_int must be available for this operation")]
    ConfGetIntUnavailable,
    #[error("conf_get_str must be available for this operation")]
    ConfGetStrUnavailable,
    #[error("tf_compile must be available for this operation")]
    TfCompileUnavailable,
    #[error("tf_eval must be available for this operation")]
    TfEvalUnavailable,
    #[error("streamer_get_playing_track_safe must be available for this operation")]
    StreamerGetPlayingTrackSafeUnavailable,
    #[error("plt_get_curr must be available for this operation")]
    PltGetCurrUnavailable,

    #[error("title formatting result must be valid UTF-8")]
    Utf8(#[from] str::Utf8Error),
    #[error("output path must be writable")]
    Io(#[from] io::Error),
}
